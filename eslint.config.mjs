import globals from "globals";
import path from "node:path";
import { fileURLToPath } from "node:url";
import js from "@eslint/js";
import { FlatCompat } from "@eslint/eslintrc";

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);
const compat = new FlatCompat({
    baseDirectory: __dirname,
    recommendedConfig: js.configs.recommended,
    allConfig: js.configs.all
});

export default [{
    ignores: ["**/*.min.js", "src/js/d3/"],
}, ...compat.extends("standard"), {
    languageOptions: {
        globals: {
            ...globals.browser,
            d3: true,
            bulmaSlider: true,
            selectedYear: true,
            selectedIndicator: true,
            selectedIndicatorScatter: true,
            YEAR_COLUMN: true,
            COUNTRY_CODE_COLUMN: true,
            COUNTRY_NAME_COLUMN: true,
            POPULATION_COLUMN: true,
            PIB_COLUMN: true,
            CSV_DATA: true,
            CSV_HEADERS: true,
            CSV_VALUES: true,
            WORLD_MAP_JSON: true,
            BULMA_COLORS: true,
            WorldHeatMap: true,
            CountryManagement: true,
            Chart: true,
            LineChart: true,
            ScatterChart: true,
            BarChart: true,
            displayNotification: true,
            removeNotificationElement: true,
            parseCSV: true,
            initIndicators: true,
            updateYearRange: true,
            initYearRange: true,
        },

        ecmaVersion: 12,
        sourceType: "script",
    },

    rules: {},
}];