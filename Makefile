.PHONY: release

release:
	$(eval version:=$(shell cat VERSION))
	echo Releasing version $(version)
	git commit VERSION -m "Version $(version)"
	git tag $(version)
	git push origin master $(version)
