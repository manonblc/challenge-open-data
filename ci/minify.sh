#!/bin/bash
mkdir -p dist

# Copy images
cp -r src/img/ dist/img/

# Minify HTML
npm install -g html-minifier
cp src/index.html dist/index.html
# Remove previous scripts
sed -i '/<script/d' dist/index.html
# Add unique script
sed -i 's/<\/body>/\t<script src="index.js"><\/script>\n<\/body>/' dist/index.html
# Replace css files
sed -i '{s/print.css/print.min.css/;s/index.css/index.min.css/}' dist/index.html
html-minifier --collapse-whitespace --remove-comments dist/index.html --output dist/index.html

# Minify CSS
npm install -g css-minify
mkdir -p dist/css
# Copy previous minified css files
cp src/css/*.min.* dist/css
css-minify --dir src/css --output dist/css

# Minify JS
npm install -g google-closure-compiler
google-closure-compiler --js src/js/*.js --js_output_file dist/index.js --warning_level QUIET
